Source: r-bioc-saturn
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-saturn
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-saturn.git
Homepage: https://bioconductor.org/packages/satuRn/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-locfdr,
               r-bioc-summarizedexperiment,
               r-bioc-biocparallel,
               r-bioc-limma,
               r-cran-pbapply,
               r-cran-ggplot2,
               r-cran-boot,
               r-cran-matrix
Testsuite: autopkgtest-pkg-r

Package: r-bioc-saturn
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: scalable analysis of differential transcript usage for RNA-sequencing
 This Bioconductor package can be used for Scalable Analysis of Differential
 Transcript Usage for Bulk and Single-Cell RNA-sequencing.
 .
 Applications satuRn provides a higly
 performant and scalable framework for performing differential
 transcript usage analyses. The package consists of three main
 functions. The first function, fitDTU, fits quasi-binomial
 generalized linear models that model transcript usage in different
 groups of interest. The second function, testDTU, tests for
 differential usage of transcripts between groups of interest.
 Finally, plotDTU visualizes the usage profiles of transcripts in
 groups of interest.
